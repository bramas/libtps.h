
import sys
if len(sys.argv) < 3:
    print('usage: fileToCBuffer <file> <variable_name>')
    #print('option: -o output')
    sys.exit()
filename = sys.argv[1]
var_name = sys.argv[2]

f = open(filename)
out = open(filename+'.c', 'w')

out.write('const char ')
out.write(var_name)
out.write('[] = {')

T = []
while True:
    c = f.read(1)
    if c == "":
        break
    T.append(str(ord(c)))

out.write(','.join(T))
out.write('};\n')

out.write('long long ')
out.write(var_name)
out.write('_size = '+str(len(T)))
out.write(';\n')
