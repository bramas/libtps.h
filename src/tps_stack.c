#include "tps_stack.h"
#include <stdlib.h>


typedef struct _tps_stack {
    int     *data;
    int     size;
    int     maxSize;
} _tps_stack;
typedef struct _tps_stack _tps_stack;


_tps_stack* tps_stack_create()
{
  _tps_stack *s = malloc(sizeof(_tps_stack));
  s->maxSize = 5;
  s->data = malloc(sizeof(int) * s->maxSize);
  s->size = 0;
  return s;
}

int tps_stack_top(_tps_stack* s)
{
    if (s->size == 0) {
        fprintf(stderr, "Error: stack empty\n");
        return -1;
    }

    return s->data[s->size-1];
}

void tps_stack_push(_tps_stack* s, int d)
{
    if (s->size < s->maxSize)
        s->data[s->size++] = d;
    else
    {
      int *newData = realloc(s->data, sizeof(int) * s->maxSize * 2);
      if(newData) {
        s->data = newData;
        s->maxSize *= 2;
        s->data[s->size++] = d;
      }
      else
      {
        fprintf(stderr, "Error: stack full\n");
      }
    }
}

int tps_stack_pop(_tps_stack* s)
{
    if (s->size == 0) {
      fprintf(stderr, "Error: stack empty\n");
      return 0;
    }
    s->size--;
    return s->data[s->size];
}

int tps_stack_size(_tps_stack* s)
{
  return s->size;
}

void tps_stack_free(_tps_stack* s)
{
  if(s)
    free(s->data);
  free(s);
}



void repondre(int q, char * res) {
  printf("%i %s\n",q , res);
}