
//#define TPS_IN_BROWSER

#ifndef LIBTPS_H_INCLUDE
#define LIBTPS_H_INCLUDE

#define _DEFAULT_SOURCE

#define true 1
#define false 0

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <float.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>


#include "tps_noise.h"
#include "tps_gui.h"
#include "tps_stack.h"
#include "tps_utils.h"


#define ERROR_GET_STRING_INPUT "La lecture d'une chaine de caractère peut provoquer une erreur (voir la documentation de tps_getString())"



#endif
