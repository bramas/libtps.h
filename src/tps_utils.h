/**
= Librairie TPS - Fonctions utiles
Quentin Bramas <bramas@unistra.fr>

*/


#ifndef LIBTPS_UTILS_H_INCLUDE
#define LIBTPS_UTILS_H_INCLUDE



#define TPS_MAIN int main(int argc, char **argv)

#define TPS_MAIN_WINDOW \
void TPS_main_window_call();\
int main(int argc, char **argv) \
{\
  TPS_createWindow();\
  while(TPS_updateOrQuit())\
    TPS_main_window_call();\
  return 0;\
}\
void TPS_main_window_call()





void _print_memory_byte(void * address);

/**
=== Description
Affiche l'octet présent dans la mémoire à une adresse donnée
*/
void print_memory_byte(void * address);


/**
=== Description
Affiche les octets présent dans la mémoire à une adresse donnée

=== Paramètres
* `address`: l'adresse de la mémoire à afficher.
* `bytes`: le nombre d'octets à afficher à partir de l'adresse fournie
*/
void print_memory_bytes(void * address, int bytes);

#endif
