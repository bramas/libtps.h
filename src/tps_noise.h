
#ifndef NOISE2_H_INCLUDE
#define NOISE2_H_INCLUDE


//---------------------------------------------------------------------
/** 
 * 1D float Perlin noise, SL "noise()"
 */
float noise1D( float x );

//---------------------------------------------------------------------
/** 
 * 1D float Perlin periodic noise, SL "pnoise()"
 */
float pnoise1D( float x, int px );

//---------------------------------------------------------------------
/** 
 * 2D float Perlin noise.
 */
float noise2D( float x, float y );

//---------------------------------------------------------------------
/** 
 * 2D float Perlin periodic noise.
 */
float pnoise2D( float x, float y, int px, int py );


//---------------------------------------------------------------------
/** 
 * 3D float Perlin noise.
 */
float noise3D( float x, float y, float z );


//---------------------------------------------------------------------
/** 
 * 3D float Perlin periodic noise.
 */
float pnoise3D( float x, float y, float z, int px, int py, int pz );


/**
 * Other version of the perlin noise 3D
 * */
float perlin3D(float x, float y, float z, float freq, int depth);
/**
 * Other version of the periodic perlin noise 3D
 * */
float perlinPeriodic3D(float x, float y, float z, float px, float py, float pz, float freq, int depth);


//---------------------------------------------------------------------
/** 
 * 4D float Perlin noise.
 */

float noise4D( float x, float y, float z, float w );

//---------------------------------------------------------------------
/** 4D float Perlin periodic noise.
 */

float pnoise4D(float x, float y, float z, float w, int px, int py, int pz, int pw);

#endif
//---------------------------------------------------------------------
