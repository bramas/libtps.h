
#define STACK_INITIAL_SIZE 10

typedef char kb_fruit;

struct kb_bag;
typedef struct kb_bag kb_bag;


void kb_bag_free(kb_bag *S);
kb_bag * kb_bag_create();
kb_bag * kb_bag_createInfinite(kb_fruit f);

int kb_bag_isEmpty(kb_bag *S);
int kb_bag_size(kb_bag *S);
int kb_bag_nonInfiniteSize(kb_bag *S);
kb_fruit kb_bag_infiniteFruit(kb_bag *S);

kb_fruit kb_bag_topFruit(kb_bag *S);

void kb_bag_putFruit(kb_bag *S, kb_fruit d);

kb_fruit kb_bag_getFruit(kb_bag *S);

