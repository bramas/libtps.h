
#include "kirby-bag.h"
#include <stdio.h>
#include <stdlib.h>


struct kb_bag {
    kb_fruit*     data;
    int     size;
    int     maxSize;
    int isInfinite;
    kb_fruit infiniteFruit;
};


void kb_bag_free(kb_bag *S)
{
  free(S->data);
  free(S);
}
kb_bag * kb_bag_create()
{
    kb_bag *S = (kb_bag*) malloc(sizeof(kb_bag));
    S->data = (kb_fruit*) malloc(sizeof(kb_fruit) * STACK_INITIAL_SIZE);
    S->size = 0;
    S->maxSize = STACK_INITIAL_SIZE;
    S->isInfinite = 0;
    return S;
}
kb_bag * kb_bag_createInfinite(kb_fruit f)
{
    kb_bag *S = (kb_bag*) malloc(sizeof(kb_bag));
    S->data = (kb_fruit*) malloc(sizeof(kb_fruit) * STACK_INITIAL_SIZE);
    S->size = 0;
    S->maxSize = STACK_INITIAL_SIZE;
    S->isInfinite = 1;
    S->infiniteFruit = f;
    return S;
}

int kb_bag_isEmpty(kb_bag *S)
{
    return !S->isInfinite && !S->size;
}
int kb_bag_size(kb_bag *S)
{
  if(S->isInfinite)
    return -1;
  return S->size;
}
int kb_bag_nonInfiniteSize(kb_bag *S)
{
  return S->size;
}
kb_fruit kb_bag_infiniteFruit(kb_bag *S){
  return S->infiniteFruit;
}
kb_fruit kb_bag_topFruit(kb_bag *S)
{
    if (S->size == 0) {
      if (S->isInfinite) {
        return S->infiniteFruit;
      }
      fprintf(stderr, "Error: bag empty\n");
      return 0;
    }

    return S->data[S->size-1];
}

void kb_bag_putFruit(kb_bag *S, kb_fruit d)
{
    if (S->size >= S->maxSize){
      int oldSize = S->maxSize;
      S->maxSize *= 2;
      if(oldSize >= S->maxSize){
        fprintf(stderr, "Error: maximum size of bag reached\n");
        return;
      }
      kb_fruit* newData = (kb_fruit*) realloc(S->data, S->maxSize);
      if(!S->data){
        fprintf(stderr, "Error: maximum size of bag reached\n");
        return;
      }
      S->data = newData;
    }

    S->data[S->size++] = d;
}

kb_fruit kb_bag_getFruit(kb_bag *S)
{

    if (S->size == 0) {
      if (S->isInfinite) {
        return S->infiniteFruit;
      }
      fprintf(stderr, "Error: bag empty\n");
    }
    else
        S->size--;
    return S->data[S->size];
}
