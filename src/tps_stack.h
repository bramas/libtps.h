/**
= Librairie TPS - Structure de données: Pile
Quentin Bramas <bramas@unistra.fr>

La libraire tps contient aussi des fonctions permettant de créer et manipuler des *piles* contenant des entiers.

**Pour pouvoir être utilisée :**
* il faut inclure le fichier d'entête : `tps_stack.h`
* lier les librairies suivantes: `-ltps -lSDL2 -lSDL2_ttf`


=== Exemple


```c
tps_stack maPile = tps_stack_create();
tps_stack_push(maPile, 15);
tps_stack_push(maPile, 32);
tps_stack_push(maPile, 53);
printf("%i\n", tps_stack_top(maPile)); // Affiche 53
printf("%i\n", tps_stack_pop(maPile)); // Affiche 53
printf("%i\n", tps_stack_pop(maPile));// Affiche 32

tps_stack_free(maPile);
```

*/

#ifndef LIBTPS_STACK_H_INCLUDE
#define LIBTPS_STACK_H_INCLUDE

#include <stdio.h>
#include <stdlib.h>



struct _tps_stack;
typedef struct _tps_stack* tps_stack;


/**
=== Description
  Création d'une pile

  Retourne une structure `tps_stack`
*/
tps_stack tps_stack_create();

/**
=== Description
Retourne l'élément se trouvant sur le sommet de la pile.

=== Paramètres
* `S`: la pile de type `tps_stack` à considérer.
*/
int tps_stack_top(tps_stack S);
/**
=== Description
Ajoute un élément sur le sommet de la pile.

=== Paramètres
* `S`: la pile de type `tps_stack` à considérer.
*/
void tps_stack_push(tps_stack S, int d);
/**
=== Description
Enlève l'élément se trouvant sur le sommet de la pile et le retourne.

=== Paramètres
* `S`: la pile de type `tps_stack` à considérer.
*/
int tps_stack_pop(tps_stack S);

/**
=== Description
Retourne la taille de la pile (le nombre d'élément s'y trouvant).

=== Paramètres
* `S`: la pile de type `tps_stack` à considérer
*/
int tps_stack_size(tps_stack S);

/**
=== Description
Libère la pille

=== Paramètres
* `S`: la pile de type `tps_stack` à considérer
*/
void tps_stack_free(tps_stack s);

void repondre(int q, char * res);

#endif
