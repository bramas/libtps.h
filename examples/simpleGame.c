#include <SDL2/SDL.h>
#include <stdio.h>
#include <tps.h>
#include <kirby.h>

void moveBackward() {
  turnLeft();
  turnLeft();
  move();
  turnLeft();
  turnLeft();
}
void moveTwoFruitsInFront() {
  move();
  putFruit();
  putFruit();
  moveBackward();
}
void moveAllFruitsBehind() {
  while(isOnFruit()) {
    getFruit();
    moveBackward();
    putFruit();
    move();
  }
}

int main(int argc, char* argv[])
{

  createWorld("map1.kirby-map");
  applyOptionsFromArgv(argc, argv);


  while(isOnFruit()) {
    getFruit();
    moveTwoFruitsInFront();
  }
  move();
  moveAllFruitsBehind();


  cleanWorld();
}
