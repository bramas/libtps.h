#include <SDL2/SDL.h>
#include <stdio.h>
#include <tps.h>

#define WIDTH 1024L
#define HEIGHT 820L
#define BOX_SIZE 10
#define PARTICULE_COUNT 10000
#define MAX_SPEED 1
#define RES_MULT 1

typedef struct Particule_ {
  double x;
  double y;
  double speedX;
  double speedY;
  double forceX;
  double forceY;
} Particule;

int main(void)
{
  tps_createWindow("test", WIDTH, HEIGHT);

  tps_texture_t * texture = tps_createTexture(WIDTH, HEIGHT);
  

  Particule particules[PARTICULE_COUNT];

  for(int i = 0; i < PARTICULE_COUNT; ++i){
    particules[i].x = rand()%WIDTH;
    particules[i].y = rand()%HEIGHT;
    particules[i].speedX = 0;
    particules[i].speedY = 0;
    particules[i].forceX = 0;
    particules[i].forceY = 0;
  }

tps_background(255,255,255);
float xdiff = 0;
  while(tps_isRunning()) {

    tps_background(255,255,255);
    tps_setColor(255, 0, 0);
    float flowMap[WIDTH / BOX_SIZE][HEIGHT / BOX_SIZE];
    for(int x = 0; x < WIDTH / BOX_SIZE; ++x){
      for(int y = 0; y < HEIGHT / BOX_SIZE; ++y){
        flowMap[x][y] = perlinPeriodic3D(x,y,xdiff,WIDTH,HEIGHT,100000,0.01,8)*3.1415*2*4;
      }
    }
    for(int i = 0; i < PARTICULE_COUNT; ++i){
      float angle = flowMap[(long long)(particules[i].x/BOX_SIZE)][(long long)(particules[i].y/BOX_SIZE)];

      particules[i].speedX += cos(angle)/10.0;
      particules[i].speedY += sin(angle)/10.0;
      double speed = sqrt(particules[i].speedX*particules[i].speedX + particules[i].speedY*particules[i].speedY);

      if(speed > MAX_SPEED) {
        particules[i].speedX *= MAX_SPEED/speed;
        particules[i].speedY *= MAX_SPEED/speed;
      }

      particules[i].x += particules[i].speedX;
      particules[i].y += particules[i].speedY;

      while(particules[i].x > WIDTH) particules[i].x -= WIDTH;
      while(particules[i].x < 0) particules[i].x += WIDTH;
      while(particules[i].y > HEIGHT) particules[i].y -= HEIGHT;
      while(particules[i].y < 0) particules[i].y += HEIGHT;

      long long index = (long long)(particules[i].x*RES_MULT) + WIDTH*RES_MULT*(long long)(floor(particules[i].y*RES_MULT));
      if((texture->pixels[index] & 0xFF) > RES_MULT-1)
        texture->pixels[index] -= RES_MULT + (RES_MULT<<8) + (RES_MULT<<16);

    }
    xdiff+=0.08;

    tps_setColor(0,255,255);
    tps_drawTexture(texture);
    
    /* uncomment to see the vector field
    for(int x = 0; x < WIDTH / BOX_SIZE; ++x){
      for(int y = 0; y < HEIGHT / BOX_SIZE; ++y){
        tps_setColor(0,0,0);
        SDL_RenderDrawLine(tps_getRenderer(),
                       x*BOX_SIZE,
                       y*BOX_SIZE,
                       x*BOX_SIZE+cos(flowMap[x][y])*BOX_SIZE,
                       y*BOX_SIZE+sin(flowMap[x][y])*BOX_SIZE);
      }
    }*/

    tps_render();

  }
  tps_freeTexture(texture);
  return 0;
}
