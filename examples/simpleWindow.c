#include <SDL2/SDL.h>
#include <stdio.h>
#include <tps.h>



int main(void)
{
  tps_createWindow("test", 800, 600);

  while(tps_isRunning()) {
    tps_background(255,255,255);
    tps_setColor(255, 0, 0);
    int x = 0;
    int y = 0;
    tps_getMousePosition(&x, &y);
    tps_drawRect(x, y, 60, 60);
    tps_drawFPS();
    tps_render();
  }
  return 0;
}
