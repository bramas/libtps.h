rm -rf /tmp/libtps.h_install_tmpdir > /dev/null 2>&1
mkdir -p /tmp/libtps.h_install_tmpdir
cd /tmp/libtps.h_install_tmpdir
wget https://gitlab.com/bramas/libtps.h/-/archive/master/libtps.h-master.tar.gz
tar -zxf libtps.h-master.tar.gz
cd /tmp/libtps.h_install_tmpdir/libtps.h-master
make
make install
echo "#!/bin/bash
curl https://gitlab.com/bramas/libtps.h/raw/master/install.sh | bash -" > /usr/local/bin/update-libtps
chmod +x /usr/local/bin/update-libtps
