import re, sys

if len(sys.argv) < 3:
    print("Usage docgen FILE.H OUTPUT")
    sys.exit()



filename = sys.argv[1]
outFilename = sys.argv[2]

f = open(filename, 'rt', encoding="utf-8")
if f is None:
    print("File "+filename+" not found")
    sys.exit(1)

buffer = None
docObjects = []

WAITING_COMMENT, BUFFERING_COMMENT, WAITING_FUNCTION = range(3)

state = WAITING_COMMENT
for l in f:
    if state == WAITING_COMMENT:
        if l[:3] == '/**':
            buffer = l[3:]
            state = BUFFERING_COMMENT
    elif state == BUFFERING_COMMENT:
        if l[:2] == '*/':
            state = WAITING_FUNCTION
        else:
            buffer += l
    elif state == WAITING_FUNCTION:
        #if l.strip() != "":
            state = WAITING_COMMENT
            docObjects.append({
                "comment": buffer,
                "function": l.strip()
            })

out = open(outFilename, 'wt', encoding="utf-8")
out.write(':source-highlighter: highlightjs\n:toclevels: 1\n:toc-title: Index\n:toc: left\n')
for o in docObjects:
    if o['function'] != '':
        m = re.search('(\w+) (\w+)\([^\)]*\)', o['function'])
        if m is not None:
            fname = m.group(2)
            out.write('[['+fname+','+fname+']]\n')
            out.write('== Fonction `'+fname+'`\n')
            out.write('[source, C]\n----\n')
            out.write(o['function'])
            out.write('\n----\n')

    out.write(o['comment']+'\n')
