
Easy install (ubuntu)
```
sudo apt-get update
sudo apt-get install -y libsdl2-dev libsdl2-ttf-dev
curl https://gitlab.com/bramas/libtps.h/raw/master/install.sh | sudo bash -
echo 'export LD_LIBRARY_PATH="/usr/local/lib"' >> ~/.bashrc
source ~/.bashrc
```

[Documentation](https://bramas.gitlab.io/libtps.h/)